package com.medianochelabs.demos.microdevops.dao;

import com.medianochelabs.demos.microdevops.model.Product;
import org.springframework.stereotype.Repository;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    @Override
    public Product getProduct() {
        Product p = new Product();
        p.setType("diamond");
        return p;
    }
}
