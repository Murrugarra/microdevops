package com.medianochelabs.demos.microdevops.dao;

import com.medianochelabs.demos.microdevops.model.Product;

public interface ProductRepository {
    Product getProduct();
}
