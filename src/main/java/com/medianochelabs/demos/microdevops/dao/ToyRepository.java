package com.medianochelabs.demos.microdevops.dao;

import com.medianochelabs.demos.microdevops.model.Toy;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ToyRepository {
    private List<Toy> toys = new ArrayList<>();

    public ToyRepository() {
        toys.add(new Toy("Forky", 529.0, 2));
        toys.add(new Toy("Buzz", 39.9, 1));
        toys.add(new Toy("Jessy", 28.9, 1));
        toys.add(new Toy("Woody", 59.9,1));
        toys.add(new Toy("Betty", 45.9, 1));
        toys.add(new Toy("Rex", 16.9, 1));
    }

    public List<Toy> findAll() {
        return toys;
    }

    public Toy findByName(String toyName) {
        return toys
                .stream()
                .filter(toy -> toy.getName().equalsIgnoreCase(toyName))
                .findFirst().orElse(null);
    }

    public void save(Toy toy) {
        toy.setVersion(toy.getVersion()+1);
    }
}
