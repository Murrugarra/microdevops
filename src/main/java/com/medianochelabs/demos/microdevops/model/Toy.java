package com.medianochelabs.demos.microdevops.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Toy {
    private String name;
    private Double price;
    private int version;
}
