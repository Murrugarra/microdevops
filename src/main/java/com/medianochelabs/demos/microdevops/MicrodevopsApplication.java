package com.medianochelabs.demos.microdevops;

import com.medianochelabs.demos.microdevops.dao.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrodevopsApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(MicrodevopsApplication.class, args);
	}

	@Autowired
	private ProductRepository productRepository;

	@Override
	public void run(String... args) throws Exception {

		System.out.println(productRepository.getProduct().getType());

	}

}
