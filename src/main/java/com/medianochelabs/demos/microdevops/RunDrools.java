package com.medianochelabs.demos.microdevops;

import com.medianochelabs.demos.microdevops.model.Product;
import org.drools.compiler.compiler.DroolsParserException;
import org.drools.compiler.compiler.PackageBuilder;
import org.drools.core.RuleBase;
import org.drools.core.RuleBaseFactory;
import org.drools.core.WorkingMemory;
import org.drools.core.rule.Package;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;


//TODO Add Kie container
public class RunDrools {

//    public static void main(String[] args) throws IOException, DroolsParserException {
//
//        new RunDrools().executeDrools();
//
//    }

    public void executeDrools() throws IOException, DroolsParserException {
        PackageBuilder packageBuilder = new PackageBuilder();

        String ruleFile = "/rules/Rules.drl";

        InputStream resourceAsStream = getClass().getResourceAsStream(ruleFile);

        Reader reader = new InputStreamReader(resourceAsStream);
        packageBuilder.addPackageFromDrl(reader);

        Package rulesPackage = packageBuilder.getPackage();
        RuleBase ruleBase = RuleBaseFactory.newRuleBase();
        ruleBase.addPackage(rulesPackage);

        WorkingMemory workingMemory = ruleBase.newStatefulSession();

        Product product = new Product();
        product.setType("diamond");

        workingMemory.insert(product);
        workingMemory.fireAllRules();

        System.out.println(product.getDiscount());
    }
}
