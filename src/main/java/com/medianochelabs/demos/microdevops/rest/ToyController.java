package com.medianochelabs.demos.microdevops.rest;

import com.medianochelabs.demos.microdevops.annotation.ValueObject;
import com.medianochelabs.demos.microdevops.dao.ToyRepository;
import com.medianochelabs.demos.microdevops.model.Toy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/toys")
public class ToyController {
    private ToyRepository toyRepository;

    public ToyController(ToyRepository aToyRepository) {
        this.toyRepository = aToyRepository;
    }

    @GetMapping
    @ValueObject(name = "Toy", detail = "Test Toy")
    public List<Toy> getAllToys() {
        return toyRepository.findAll();
    }

    @GetMapping("/{toyName}")
    public ResponseEntity<Toy> getToy(@PathVariable String toyName) {
        Toy foundToy = toyRepository.findByName(toyName);
        return ResponseEntity
                .ok()
                .eTag(String.valueOf(foundToy.getVersion()))
                .body(foundToy);
    }

    @PutMapping(path = "/{toyName}")
    public ResponseEntity updateToy(@PathVariable String toyName,
                                    @RequestHeader(name = HttpHeaders.IF_MATCH) String version,
                                    @RequestBody Toy newToy) {

        Toy foundToy = toyRepository.findByName(toyName);
        if (foundToy == null) {
            return ResponseEntity.notFound().build();
        }

        if (!String.valueOf(foundToy.getVersion()).equalsIgnoreCase(version)) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        foundToy.setName(newToy.getName());
        foundToy.setPrice(newToy.getPrice());
        toyRepository.save(foundToy);
        return ResponseEntity
                .ok()
                .eTag(String.valueOf(foundToy.getVersion()))
                .body(foundToy);
    }
}
