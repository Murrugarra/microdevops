package com.medianochelabs.demos.microdevops.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

import javax.servlet.Filter;

@Configuration
public class RestConfig {

    @Bean
    public ShallowEtagHeaderFilter eTagFilter() {
        return new ShallowEtagHeaderFilter();
    }
}
