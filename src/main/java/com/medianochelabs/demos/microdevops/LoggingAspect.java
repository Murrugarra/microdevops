package com.medianochelabs.demos.microdevops;

import com.medianochelabs.demos.microdevops.model.Product;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Aspect
public class LoggingAspect {
    private Logger logger = Logger.getLogger(LoggingAspect.class.getName());

    @Around("execution(* com.medianochelabs.demos.microdevops.dao.*.*(..))")
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.info("Entering Method " + joinPoint.getSignature().toShortString());
        return joinPoint.proceed();
    }

    // TODO generate etags with spring boot.

//    @Before("execution(* com.medianochelabs.demos.microdevops.dao.*.*(..))")
//    public void before(JoinPoint joinPoint) {
//        logger.info("Entering Method " + joinPoint.getSignature().toShortString());
//    }
//
//    @After("execution(* com.medianochelabs.demos.microdevops.dao.*.*(..))")
//    public void after(JoinPoint joinPoint) {
//        logger.info("Existing Method " + joinPoint.getSignature().toShortString());
//    }


}
