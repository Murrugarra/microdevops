package com.medianochelabs.demos.microdevops;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DevOpsController {

    @Value("${server.port}")
    int serverPort;

    @GetMapping("/sayHello")
    public String sayHello(@RequestParam(value = "to", defaultValue = "unknow") String aName) {
        return String.format("Hello %s! This response comes from port %s", aName, serverPort);
    }
}
