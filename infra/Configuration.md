# App configuration

## Logs

* Application: logs are located at /apps/java/APP_NAME folder.
* Nginx: logs are located at /var/logs/nginx folder.

## Users
* apprunner: runs java applications.
* nginxuser: manage nginx config.
* root: el super papi.
* guest: view mode.

## Launcher
The app is created as an Operating System service.


## Connect to server
```
$ ssh -i eriksonmacmedianoche.pem ec2-user@ec2-100-26-49-112.compute-1.amazonaws.com
```

## Run Prometheus
```
$ docker run -d --name=prometheus -p 9090:9090 -v C:\mentoria\demos\microdevops\infra\prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml
```

## Run Grafana
```
$ docker run -d --name=grafana -p 3000:3000 grafana/grafana 
```
PD: default username/password is admin/admin.

## Command for service
```
$ systemctl --type=service
```
Service should be pasted in folder: /etc/systemd/system.
Using service:
```
$ systemctl daemon-reload
$ systemctl enable service_name
$ systemctl [start|stop|reload] service_name
```

## User And Groups
Detail:  id RES.
List members of group: 
```
$ cat /etc/passwd | awk -F':' '{ print $1}' | xargs -n1 groups
```
List all groups /etc/group

test:
          /blog
autor     wrx      
editor    wr      
viewer    r

change user group: 
```
$ usermod -g GROUP USER
```

Change owner for user 
```
$ chown -R USER RES
```

Mix:
```
$ chown USER:GROUP RES
```
Change permissions: 
```
$ chmod -R 000 RES
```
